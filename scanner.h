//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 2
//Date:     11/18/19
//-----------------------------------------------------------------------------

#ifndef INC_4280_P2_SCANNER_H
#define INC_4280_P2_SCANNER_H

#include "token.h"
#include <vector>


using namespace std;

//Table size
const int STATES = 4; //rows
const int INPUTS = 6; //columns
//input file string
extern vector<string> file_string;

/*  Possible states:
 *  Represent the 2-D array for the FSA as array of integers
 *   -1, -2, etc could be different errors
     0, 1, etc would be states/rows
     1001, 1002, etc could be final states recognizing different tokens
 * */

const int ERROR_UPPERCASE = -1;
const int ERROR_DIGIT = -2;
const int STATE_ZERO = 0; //ID
const int STATE_ONE = 1; //Keyword
const int STATE_TWO = 2; //Op
const int STATE_THREE = 3; //INT
const int ID_FINAL_STATE = 1001;
const int INT_FINAL_SATE = 1002;
const int OP_FINAL_STATE = 1003;
const int EOF_FINAL_STATE = 1004;

//Define the FSA table. See stateTable.txt for table layout.
const int FSA_TABLE[STATES][INPUTS] = {
        {STATE_TWO, ERROR_UPPERCASE, STATE_THREE, STATE_ONE, STATE_ZERO, EOF_FINAL_STATE },
        {OP_FINAL_STATE, OP_FINAL_STATE, OP_FINAL_STATE, OP_FINAL_STATE,OP_FINAL_STATE, OP_FINAL_STATE},
        {STATE_TWO, STATE_TWO, STATE_TWO, ID_FINAL_STATE, ID_FINAL_STATE, ID_FINAL_STATE},
        {ERROR_DIGIT, ERROR_DIGIT, STATE_THREE, INT_FINAL_SATE, INT_FINAL_SATE, INT_FINAL_SATE}
};


//Function Prototypes
void readFile(istream&);
int filter(string&);
bool isValidCharacter(char);
string getString();
int getColumn(char);
int scanner(Token &);
void errorOccured(int, string);


#endif //INC_4280_P2_SCANNER_H
