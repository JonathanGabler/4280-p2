# Compiler

> This is a compiler developed for CS4280 Program Translation at UMSL.

Read Project P2.pdf for project details.

## Requirements

C/C++ compiler.

## Usage Example

The included makefile can be used as such:

```sh
~$ make
```
To run the project after you build it:
```sh
~$ ./frontEnd filename
```
Where filename is the file you wish to test. Provided ones in Test-Files dir

## Grammar
Provided grammar for the project
```
<S> -> program <V> <B>
<B> -> begin <V> <Q> end
<V> -> empty | var identifier . <V>
<M> -> <H> + <M> | <H> - <M> | <H> / <M> | <H> * <M> | <H>
<H> -> & <R> | <R>
<R> -> identifier | number
<Q> -> <T> # <Q> | empty
<T> -> <A> , | <W> , | <B> | <I> , | <G> , | <E> ,
<A> -> scan identifier | scan number
<W> -> write <M>
<I> -> if [ <M> <Z> <M> ] <T>
<G> -> repeat [ <M> <Z> <M> ] <T>
<E> -> let identifier : <M>
<Z> -> < | > | : | = | = =
```
## Contents 

Here is a list of the included files and their usage in this project:

* ``` main.cpp ```
  * handles the input from either passed file or command line input, and calls parse class
* ``` node.h ```
  * data structure for use with parse tree
* ``` scanner.h & scanner.cpp ```
  * used by the parser to create tokens from the source code
* ``` parser.h & parser.cpp ```
  * checks the tokens in the parse tree to check if they conform to the requirements of the course code grammar
* ``` testTree.h & testTree.cpp ```
  * display the generated parse tree
* ``` token.h & token.cpp ```
  * Builds the structure for our tokens and creates maps for tokens

 
## About

* Author: Jonathan Gabler
* Contact: JonGabler@outlook.com jegq6b@mail.umsl.edu
* Organization: UMSL

## Known Issues
* None at the moment 

![Picture](https://gitlab.com/JonathanGabler/4280-p2/blob/master/TokenError.PNG)





