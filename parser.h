//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 2
//Date:     11/18/19
//-----------------------------------------------------------------------------

#ifndef INC_4280_P2_PARSER_H
#define INC_4280_P2_PARSER_H

#include <string>
#include "token.h"
#include "node.h"

using namespace std;

//token returned from scanner each pass
extern Token token;

//Empty token
extern Token EMPTY_TOKEN;

//Prototype for parser function
node_t *parser();

//Error found in parser
void parseError();

//create node
node_t *createNode(string);

//Language provided
node_t *S();
node_t *B();
node_t *V();
node_t *M();
node_t *H();
node_t *R();
node_t *Q();
node_t *T();
node_t *A();
node_t *W();
node_t *I();
node_t *G();
node_t *E();
node_t *Z();


#endif //INC_4280_P2_PARSER_H
